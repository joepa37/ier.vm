// Copyright (c) 2016, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.require("assets/js/charts.min.js");

let _options = {
	rotated: false,
	grouped: true,
	subchart: false,
};

frappe.query_reports["Course Assessment Report"] = {
	"filters": [
		{
			"fieldname":"assessment_group",
			"label": __("Assessment Group"),
			"fieldtype": "Link",
			"options": "Assessment Group",
			"reqd": 1,
			"get_query": function() {
				return{
					filters: {
						'is_group': 0
					}
				};
			},
		},
		{
			"fieldname":"course",
			"label": __("Course"),
			"fieldtype": "Link",
			"options": "Course",
			"reqd": 1,
		},
		{
			"fieldname":"student_group",
			"label": __("Student Group"),
			"fieldtype": "Link",
			"options": "Student Group",
		}
	],
	before_run: (report) => {
		report.page.inner_toolbar.toggle(false);
	},
	custom_chart: (report, response) => {
		$('.chart-area').empty();
		report.page.clear_inner_toolbar();

		let _resultsChart = resultsChart({results: response.chart.results});
		let _scalesChart;

		report.page.add_inner_button(__("Pie Chart"), () => {
			if(_scalesChart){
				_scalesChart.transform('pie');
			}else{
				_scalesChart = scalesChart({results: response.chart.scales, type: 'pie'});
			}
		}, __("Scale Results"));
		report.page.add_inner_button(__("Donut Chart"), () => {
			if(_scalesChart){
				_scalesChart.transform('donut');
			}else{
				_scalesChart = scalesChart({results: response.chart.scales});
			}
		}, __("Scale Results"));

		/*report.page.add_inner_button(__("Generate"), () => {
			_resultsChart = resultsChart({animate: true, results: response.chart.results});
		}, __("Students Results"));*/
		report.page.add_inner_button(__("Rotate"), () => {
			_options.rotated = !_options.rotated;
			_resultsChart = resultsChart({results: response.chart.results});
		}, __("Students Results"));
		report.page.add_inner_button(__("Group/Ungroup"), () => {
			if(_resultsChart){
				if(_options.grouped){
					_resultsChart.groups([]);
				}else{
					_resultsChart.groups(response.chart.results._groups);
				}
				_options.grouped = !_options.grouped;
			}else{
				_resultsChart = resultsChart({results: response.chart.results});
			}
		}, __("Students Results"));
		report.page.add_inner_button(__("Zoom/Sub chart"), () => {
			_options.subchart = !_options.subchart;
			_resultsChart = resultsChart({results: response.chart.results});
		}, __("Students Results"));

		report.page.inner_toolbar.toggle(true);
	},
	onload: (report) => {
		$('.chart-area').css('margin', '0');
	},
};

function resultsChart(options){
	options = options || {};
	let height = options.height || 600;
	let animate = options.animate || false;
	let grouped = options.grouped || _options.grouped;
	let rotated = options.rotated || _options.rotated;
	let subchart = options.subchart || _options.subchart;
	let results = options.results || null;

	if(results === null){
		return;
	}

	$('#c3-results-chart').remove();
	$('.chart-area').prepend('<div id="c3-results-chart"></div>');

	if(rotated){
		let new_height = results.columns[0].length * 25;
		if(new_height < height){
			new_height = height;
		}
		height = new_height;
	}
	let chart = c3.generate({
		title: {
			text: 'Students Results Chart',
		},
		bindto: '#c3-results-chart',
		size: {
			height: height,
		},
		data: results,
		subchart: {
			show: subchart
		},
		zoom: {
			enabled: !subchart
		},
		axis: {
			rotated: rotated,
			x: {
				type: 'category',
				tick: {
					rotate: 80,
					multiline: false
				},
				padding: {left: 0, right: 0.5}
			},
			y: {
				label: {
					text: 'Assessment Criterias',
					position: 'outer-middle'
				}
			 },
		},
		tooltip: {
			contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
				let $$ = this, config = $$.config,
				titleFormat = config.tooltip_format_title || defaultTitleFormat,
				nameFormat = config.tooltip_format_name || function (name) { return name; },
				valueFormat = config.tooltip_format_value || defaultValueFormat,
				text, i, title, value, name, bgcolor;
				for (i = 0; i < d.length; i++) {
					if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

					if (! text) {
						title = titleFormat ? titleFormat(d[i].x) : d[i].x;
						text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
						text += "<tr><td colspan='2' style='font-weight: 600; text-align: center'>" + results.scales[d[i].index] + "</td></tr>";
					}

					name = nameFormat(d[i].name);
					value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
					bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

					text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
					text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
					text += "<td class='value'>" + value + "</td>";
					text += "</tr>";
				}
				return text + "</table>";
			}
		},
		grid: {
			y: {
				lines: [
					{value: 20, text: '20'},
					{value: 40, text: '40'},
					{value: 60, text: '60'},
					{value: 80, text: '80'},
					{value: 100, text: '100'},
				]
			}
		},
		color: {
			pattern: ['#FF462F', '#B2CC53', '#FFD34B', '#5C89FF', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'],
		}
	});

	if(grouped){
		if(animate){
			setTimeout(function () {
				chart.groups(results._groups);
				chart.transform('spline', 'total_score');
			}, 1000);
		}else{
			chart.groups(results._groups);
			chart.transform('spline', 'total_score');
		}
	}else{
		chart.transform('spline', 'total_score');
	}

	return chart;
}


function scalesChart(options){
	options = options || {};
	let height = options.height || 500;
	let results = options.results || null;
	let type = options.type || 'donut';

	if(results === null){
		return;
	}

	$('#c3-scales-chart').remove();
	$('.chart-area').prepend('<div id="c3-scales-chart"></div>');

	let chart = c3.generate({
		title: {
			text: 'Scales Results Chart',
		},
        bindto: '#c3-scales-chart',
		size: {
			height: height,
		},
		data: results,
		color: {
			pattern: ['#FF462F', '#D65D94', '#5C89FF', '#98df8a', '#2ca02c', '#FFD34B', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5'],
		},
		tooltip: {
			contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
				let $$ = this, config = $$.config,
				titleFormat = config.tooltip_format_title || defaultTitleFormat,
				nameFormat = config.tooltip_format_name || function (name) { return name; },
				valueFormat = config.tooltip_format_value || defaultValueFormat,
				text, i, title, value, name, bgcolor;
				for (i = 0; i < d.length; i++) {
					if (! (d[i] && (d[i].value || d[i].value === 0))) { continue; }

					if (! text) {
						title = titleFormat ? titleFormat(d[i].x) : d[i].x;
						text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
					}

					name = nameFormat(d[i].name);
					value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
					bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

					text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
					text += "<td class='name' colspan='2' style='font-weight: 600;'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
					text += "</tr>";

					text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
					text += "<td class='name'>Percentage</td>";
					text += "<td class='value'>" + value + "</td>";
					text += "</tr>";

					text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
					text += "<td class='name'>Quantity</td>";
					text += "<td class='value'>" + d[i].value + (d[i].value > 1 ? " Students" : " Student") + "</td>";
					text += "</tr>";

					text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
					text += "<td class='name' colspan='2' style='text-align: center; font-weight: 600;'>" + (d[i].value > 1 ? " Students" : " Student") + "</td>";
					text += "</tr>";

					for(let j = 0; j < results.scales[d[i].name].length; j++){
						if(j < 15){
							text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
							text += "<td class='name' colspan='2'>" + results.scales[d[i].name][j] + "</td>";
							text += "</tr>";
						}else{
							let rest = results.scales[d[i].name].length - j;
							text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
							text += "<td class='name' colspan='2' style='font-weight: 600;'>" + rest + " more " + (rest > 1 ? " Students" : " Student") + "...</td>";
							text += "</tr>";
							break;
						}
					}
				}
				return text + "</table>";
			}
		},
    });

	chart.transform(type);
	return chart;
}